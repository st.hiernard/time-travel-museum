﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
///     Cette classe contient tout le comportement des affiches.
/// </summary>
public class PosterBehaviour : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit du clip audio qui sera joué quand des notes seront prises proche de l'affiche courante.
    /// </summary>
    public AudioClip noteAudio;

    /// <summary>
    ///     Il s'agit du texte qui sera affiché sur l'affiche.
    /// </summary>
    [TextArea] public string textToShow = "text...";

    /// <summary>
    ///     Il s'agit de la liste de notes (string) pouvant être prises sur l'affiche courante.
    /// </summary>
    public List<string> listNotesToShow = new List<string>();

    /// <summary>
    ///     Il s'agit du composant AudioSource qui jouera le clip <see cref="noteAudio" />.
    /// </summary>
    private AudioSource _audioSource;

    /// <summary>
    ///     Il s'agit du composant permettant de gérer l'affichage des notes sur le panneau d'affichage.
    /// </summary>
    private NotesManager _notesManager;

    /// <summary>
    ///     Il permet de savoir si les notes ont été affichées ou non.
    /// </summary>
    private bool _notesTaken;

    /// <summary>
    ///     Il s'agit de la zone de texte utilisée pour afficher le texte principal.
    /// </summary>
    private TextMeshPro _textMesh;


    // Start is called before the first frame update
    private void Start()
    {
        // On récupère les composants utiles
        _textMesh = gameObject.GetComponentInChildren<TextMeshPro>();
        _audioSource = GetComponent<AudioSource>();
        var notesManagersGO = GameObject.Find("NotesManager");
        if (notesManagersGO) _notesManager = notesManagersGO.GetComponent<NotesManager>();
        // On affiche le texte indiqué dans l'inspecteur.
        _textMesh.text = textToShow;
        // On indique au composant AudioSource le clip à utiliser.
        _audioSource.clip = noteAudio;
    }

    /// <summary>
    ///     Cette méthode permet de prendre des notes.
    /// </summary>
    /// <remarks>
    ///     Si les notes ne sont pas encore prises,
    ///     elle met à jour la liste des notes du <see cref="_notesManager" />,
    ///     et joue un son pour signaler à l'utilisateur qu'une note a été prise.
    /// </remarks>
    public void TakeNotes()
    {
        if (_notesManager && !_notesTaken)
        {
            _notesManager.UpdateList(listNotesToShow);
            _notesTaken = true;
            _audioSource.Play();
        }
    }
}