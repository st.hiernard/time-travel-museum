﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
///     Cette classe permet de gérer le système de prise de notes.
/// </summary>
public class NotesManager : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit de la zone de texte utilisée pour afficher les notes.
    /// </summary>
    public TextMeshPro textMeshPro;

    /// <summary>
    ///     Il s'agit de la liste des notes (string).
    /// </summary>
    private readonly List<string> _notesList = new List<string>();

    /// <summary>
    ///     Il s'agit du texte à afficher dans la zone de texte <see cref="textMeshPro" />.
    /// </summary>
    private string _textToShow = "";

    /// <summary>
    ///     Il s'agit de l'instance du NotesManager (singleton).
    /// </summary>
    private static NotesManager Instance { get; set; }

    // Start is called before the first frame update
    private void Start()
    {
        /* On vérifie qu'une instance du composant n'existe pas déjà.
            Si c'est le cas on détruit le nouveau composant, sinon on continue.
         */
        if (Instance == null)
        {
            Instance = this;
            // On indique qu'il ne faudra pas détruire le GameOject courant lorsqu'on chargera une nouvelle scène.
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    private void Update()
    {
        // Si la zone de texte est déjà définie, on l'utilise...
        if (textMeshPro)
            textMeshPro.text = _textToShow;
        // Sinon, on la recherche...
        else
            GetTextMeshPro();
    }

    /// <summary>
    ///     Cette méthode permet de rechercher la bonne zone de texte à utiliser.
    /// </summary>
    private void GetTextMeshPro()
    {
        var notesZone = GameObject.Find("NotesZone");
        if (notesZone)
        {
            var content = notesZone.transform.Find("Content");
            if (content)
            {
                var meshPro = content.GetComponent<TextMeshPro>();
                if (meshPro) textMeshPro = meshPro;
            }
        }
    }

    /// <summary>
    ///     Cette méthode permet de mettre à jour <see cref="_notesList" /> et <see cref="_textToShow" />
    ///     en prenant en paramètre la liste des notes à ajouter (<paramref name="notes" />.
    /// </summary>
    /// <param name="notes"></param>
    public void UpdateList(List<string> notes)
    {
        foreach (var noteText in notes)
        {
            _notesList.Add(noteText);
            var i = _notesList.Count - 1;
            _textToShow += "- " + noteText + "\n";
        }
    }
}