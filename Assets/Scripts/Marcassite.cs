﻿using UnityEngine;

/// <summary>
///     Cette classe permet d'avoir un composant utile à l'identification d'une pierre de type Marcassite.
///     Si un objet (une pierre) possède ce composant, alors il est assimilé à une Marcassite,
///     notamment quand il s'agit de produire des étincelles.
/// </summary>
public class Marcassite : MonoBehaviour
{
}