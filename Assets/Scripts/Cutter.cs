﻿using UnityEngine;

/// <summary>
///     Cette classe permet d'avoir un composant utile à l'identification d'un objet assimilé à un couteau.
///     Si un objet possède ce composant, alors il est assimilé à un couteau,
///     notamment quand il s'agit de tailler une branche.
/// </summary>
public class Cutter : MonoBehaviour
{
}