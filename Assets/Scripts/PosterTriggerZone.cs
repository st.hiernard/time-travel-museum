﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     Cette classe permet de détecter quand l'utilisateur pénètre dans la zone sous les affiches du musée , et active la prise de notes.
/// </summary>
public class PosterTriggerZone : MonoBehaviour
{
    private PosterBehaviour _posterBehaviour;
    // Start is called before the first frame update
    void Start()
    {
        _posterBehaviour = GetComponentInParent<PosterBehaviour>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (_posterBehaviour) _posterBehaviour.TakeNotes();
        }
    }
}
