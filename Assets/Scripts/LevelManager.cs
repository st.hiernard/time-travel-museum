﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
///     Cette classe permet de gérer les missions du niveau "Préhistoire".
/// </summary>
/// <remarks>
///     On utilise le Design Pattern "Singleton",
///     ce qui nous permet d'avoir une seule instance durant toute l'exécution du jeu,
///     notamment après avoir rechargé les différentes scènes.
/// </remarks>
public class LevelManager : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit d'une référence vers la zone de texte utilisée pour afficher les missions.
    /// </summary>
    public TextMeshPro textMeshPro;

    /// <summary>
    ///     Il s'agit du texte qu'on affichera dans la zone de texte <see cref="textMeshPro" />.
    /// </summary>
    private string _textToShow = "";

    /// <summary>
    ///     Il s'agit de l'instance du LevelManager (singleton).
    /// </summary>
    private static LevelManager Instance { get; set; }

    /// <summary>
    ///     Il s'agit de l'état du niveau 1 (complété ou non).
    /// </summary>
    public bool IsLevel1Completed { get; set; }

    /// <summary>
    ///     Il s'agit de la liste des missions (intitulés et états).
    /// </summary>
    public List<(string, bool)> QuestsLabels { get; set; }

    // Start is called before the first frame update
    private void Start()
    {
        /* On vérifie qu'une instance du composant n'existe pas déjà.
            Si c'est le cas on détruit le nouveau composant, sinon on continue.
         */
        if (Instance == null)
        {
            Instance = this;
            // On indique qu'il ne faudra pas détruire le GameOject courant lorsqu'on chargera une nouvelle scène.
            DontDestroyOnLoad(gameObject);
            InitQuestsLabels();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    private void Update()
    {
        // Si la zone de texte est déjà définie, on l'utilise...
        if (textMeshPro)
        {
            UpdateText();
            textMeshPro.text = _textToShow;
        }
        // Sinon, on la recherche...
        else
        {
            GetTextMeshPro();
        }
    }

    /// <summary>
    ///     Cette méthode permet d'initialiser la liste des missions et d'ajouter les premières missions.
    /// </summary>
    private void InitQuestsLabels()
    {
        QuestsLabels = new List<(string, bool)>();
        QuestsLabels.Add(("Mission 1 : Allumer un feu", false));
        QuestsLabels.Add(("Mission 2 : Tailler un épieu", false));
    }

    /// <summary>
    ///     Cette méthode permet de rechercher la bonne zone de texte à utiliser.
    /// </summary>
    private void GetTextMeshPro()
    {
        var questsZone = GameObject.Find("QuestsZone");
        if (questsZone)
        {
            var content = questsZone.transform.Find("Content");
            if (content)
            {
                var meshPro = content.GetComponent<TextMeshPro>();
                if (meshPro) textMeshPro = meshPro;
            }
        }
        else
        {
            textMeshPro = null;
        }
    }

    /// <summary>
    ///     Cette méthode permet d'ajouter une nouvelle mission à la liste.
    /// </summary>
    /// <param name="quest">Le couple formé de l'intitulé et de l'état de la mission.</param>
    public void AddQuest(string quest)
    {
        if (!(QuestsLabels.Contains((quest, false)) || QuestsLabels.Contains((quest, true))))
            QuestsLabels.Add((quest, false));
    }

    /// <summary>
    ///     Cette méthode permet de mettre à jour <see cref="_textToShow" />.
    /// </summary>
    private void UpdateText()
    {
        _textToShow = "";
        foreach (var quest in QuestsLabels)
            if (quest.Item2)
                _textToShow += "- " + quest.Item1 + "- OK\n";
            else
                _textToShow += "- " + quest.Item1 + "\n";
    }
}