﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
///     Cette classe permet de gérer le comportement du bouton de téléportation dans une simulation.
/// </summary>
public class TeleportBtn : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        /* Si le joueur appuie sur le bouton (rentre dans la zone de trigger)
         alors on le téléporte dans un autre monde */
        if (other.gameObject.CompareTag("Player")) TeleportToOtherWorld();
    }

    /// <summary>
    ///     Cette méthode permet de charger la scène "PrehistoricScene".
    /// </summary>
    private void TeleportToOtherWorld()
    {
        SceneManager.LoadScene("PrehistoricScene", LoadSceneMode.Single);
    }
}