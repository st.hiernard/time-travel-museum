﻿using UnityEngine;

/// <summary>
///     Cette classe permet de gérer le comportement d'une zone de trigger (celle de la salle spéciale).
/// </summary>
public class TriggerSound : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit du composant AudioSource chargé de jouer le son.
    /// </summary>
    /// <remarks>
    ///     Le clip audio est directement renseigné dans l'inspecteur.
    /// </remarks>
    private AudioSource _audioSource;

    // Start is called before the first frame update
    private void Start()
    {
        // On récupère le composant AudioSource
        _audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        // Si le joueur rentre dans la zone, on joue un son.
        if (other.CompareTag("Player")) _audioSource.Play();
    }
}