﻿using UnityEngine;

/// <summary>
///     Cette classe permet de faire "revenir" les objets qui passent à travers le sol.
/// </summary>
/// <remarks>
///     Il suffit d'attacher ce script/composant à tous les objets auxquels on souhaite ajouter ce comportement.
/// </remarks>
public class PositionResetter : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit de la position de départ de l'objet.
    /// </summary>
    private Vector3 _startingPosition;

    // Start is called before the first frame update
    private void Start()
    {
        _startingPosition = transform.position;
    }

    /// <summary>
    ///     Cette méthode permet de réinitialiser la position de l'objet à sa position de départ.
    /// </summary>
    public void ResetPosition()
    {
        transform.position = _startingPosition;
    }
}