﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
///     Cette classe permet de gérer le comportement de la pointe de la lance (branche taillée).
/// </summary>
public class SpikeBehaviour : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit du material de la pointe.
    /// </summary>
    public Material spikeMat;

    /// <summary>
    ///     Il s'agit d'une référence au composant GameManager.
    /// </summary>
    public GameManager gameManager;

    /// <summary>
    ///     Il s'agit de la durée nécessaire pour brûler la pointe de la lance (en secondes).
    /// </summary>
    public float burningDuration;

    /// <summary>
    ///     Il s'agit de l'intervalle de temps à attendre entre chaque exécution du corp de la coroutine
    ///     <see cref="BurnSpike" />
    /// </summary>
    private float _deltaTime;

    /// <summary>
    ///     Cet attribut permet de savoir si la pointe est brûlée ou non.
    /// </summary>
    private bool _isBurned;

    /// <summary>
    ///     Cet attribut permet de savoir si le feu est allumé ou non.
    /// </summary>
    private bool _isFireOn;

    /// <summary>
    ///     Il s'agit du composent MeshRenderer contenant le material de la pointe.
    /// </summary>
    private MeshRenderer _meshRenderer;

    /// <summary>
    ///     Il s'agit de la composante *Hue* de la couleur de la pointe (du material) convertie en HSV.
    /// </summary>
    private float _spikeColorH;

    /// <summary>
    ///     Il s'agit de la composante *Saturation* de la couleur de la pointe (du material) convertie en HSV.
    /// </summary>
    private float _spikeColorS;

    /// <summary>
    ///     Il s'agit de la composante *Value* de la couleur de la pointe (du material) convertie en HSV.
    /// </summary>
    /// <remarks>
    ///     C'est cette valeur qu'on va diminuer pour obscurcir la pointe (effet de brûlé).
    /// </remarks>
    private float _spikeColorV;


    // Start is called before the first frame update
    private void Start()
    {
        // On récupère le composant MeshRenderer
        _meshRenderer = GetComponent<MeshRenderer>();
        // On convertis la couleur du la pointe en HSV
        if (spikeMat) Color.RGBToHSV(spikeMat.color, out _spikeColorH, out _spikeColorS, out _spikeColorV);
        // On arrondis la valeur de _spikeColorV à 2 chiffres après la virgule
        _spikeColorV = (float) Math.Round(_spikeColorV, 2);
        // On utilise cette nouvelle valeur pour changer la couleur de la pointe (en reconvertissant en RGB).
        var newColor = Color.HSVToRGB(_spikeColorH, _spikeColorS, _spikeColorV);
        if (_meshRenderer) _meshRenderer.material.color = newColor;
        // On calcule le _deltaTime nécessaire pour diminuer progressivement la valeur de _spikeColorV jusqu'à 0.
        _deltaTime = burningDuration / (_spikeColorV * 100 - 1);
    }

    // Update is called once per frame
    private void Update()
    {
        // On affecte l'état de la mission à _isFireOn
        if (!_isFireOn) _isFireOn = gameManager.IsMission1Completed;
    }

    public void OnTriggerEnter(Collider other)
    {
        // On déclenche la coroutine "BurnSpike" quand on rentre la pointe de la lance dans le feu.
        if (!_isBurned && _isFireOn && other.gameObject.CompareTag("FireZone")) StartCoroutine("BurnSpike");
    }


    private void OnTriggerExit(Collider other)
    {
        // On arrête la coroutine "BurnSpike" quand on sort la pointe de la lance dans le feu.
        if (other.gameObject.CompareTag("FireZone")) StopCoroutine("BurnSpike");
    }

    /// <summary>
    ///     Cette routine permet d'assombrir progressivement la pointe de la branche qui brûle.
    ///     Il s'agit de diminuer progressivement la valeur de <see cref="_spikeColorV" />.
    /// </summary>
    /// <returns></returns>
    private IEnumerator BurnSpike()
    {
        // Tant que la couleur n'est pas totalement noire...
        while (_spikeColorV > 0.00f)
        {
            // On décrémente _spikeColorV de 0.01
            _spikeColorV -= 0.01f;
            // On arrondis _spikeColorV à 2 chiffres après la virgule
            _spikeColorV = (float) Math.Round(_spikeColorV, 2);
            // On utilise cette nouvelle valeur pour changer la couleur de la pointe (en reconvertissant en RGB)
            var newColor = Color.HSVToRGB(_spikeColorH, _spikeColorS, _spikeColorV);
            if (_meshRenderer) _meshRenderer.material.color = newColor;
            // On attend _deltaTime avant de revenir dans cette coroutine.
            yield return new WaitForSeconds(_deltaTime);
        }

        // On indique que la pointe est brûlée.
        _isBurned = true;
        // On indique que la mission 2 est complétée.
        gameManager.IsMission2Completed = true;
    }
}