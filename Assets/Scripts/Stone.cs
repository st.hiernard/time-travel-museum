﻿using UnityEngine;

/// <summary>
///     Cette classe permet de gérer le comportement d'une pierre.
/// </summary>
public class Stone : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit d'une référence au composant GameManager.
    /// </summary>
    public GameObject gameManager;

    /// <summary>
    ///     Il s'agit du prefab pour les particules d'étincelles.
    /// </summary>
    public GameObject sparkPrefab;

    /// <summary>
    ///     Il s'agit ici du prefab pour les particules de poussières.
    /// </summary>
    public GameObject dustPrefab;

    /// <summary>
    ///     Il s'agit d'une référence vers le GameObject sur lequel est rattaché le composant <see cref="StoneCollisionArea" /> correspondant à la zone de création du feu
    ///     .
    /// </summary>
    public GameObject stoneCollisionArea;

    /// <summary>
    ///     Il s'agit d'une référence vers le GameObject correspondant au sol du feu de camp.
    /// </summary>
    public GameObject fireCampGround;

    /// <summary>
    ///     Il s'agit de prefab pour le feu de camp.
    /// </summary>
    public GameObject firePrefab;

    /// <summary>
    ///     Il s'agit du seuil de force pour produire des étincelles
    /// </summary>
    public float forceAmountToCreateFire = 2.5f;

    /// <summary>
    ///     Il s'agit du seuil de force pour produire de la poussiière
    /// </summary>
    public float forceAmountToCreateDust = 0.5f;

    /// <summary>
    ///     Il s'agit du clip audio en cas de collision entre deux pierres.
    /// </summary>
    public AudioClip stoneCollisionSound;

    /// <summary>
    ///     Il s'agit du clip audio en cas de production d'étincelles.
    /// </summary>
    public AudioClip sparksSound;

    /// <summary>
    ///     Il s'agit du composant AudioSource chargé de jouer les sons.
    /// </summary>
    private AudioSource _audioData;

    // Start is called before the first frame update
    private void Start()
    {
        // On récupère le composant AudioSource
        _audioData = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Stone>() != null) //Si l'objet possède le composant Stone
            ManageStoneCollisionEnter(collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        // On arrête la vibration des manettes
        OVRInput.SetControllerVibration(0f, 0f, OVRInput.Controller.Touch);
    }

    /// <summary>
    ///     Cette méthode permet de gérer la collision lorsqu'il s'agit d'un objet possédant le composant <see cref="Stone" />,
    ///     donc lorsqu'il s'agit d'une autre pierre.
    /// </summary>
    /// <param name="otherStone">L'entité de type Collision correspondant à une pierre.</param>
    private void ManageStoneCollisionEnter(Collision otherStone)
    {
        Debug.Log("Collision entre 2 pierres");

        //var collisionForce = collision.impulse.magnitude / Time.fixedDeltaTime; //Calcul de la force de collision
        var collisionForce = otherStone.relativeVelocity.magnitude; //Calcul de la vitesse de collision (On a retenue la vitesse au lieu de la force car cela fonctionnait mieux au niveau des intéractions)

        if (collisionForce > forceAmountToCreateFire && StonesAreDifferent(otherStone.gameObject)
        ) //Si on tape assez fort et que les pierres sont différentes
        {
            _audioData.PlayOneShot(sparksSound, 0.7F); //On joue un son spécifique
            foreach (var contact in otherStone.contacts) //Pour chaque point de contact
                Destroy(Instantiate(sparkPrefab, contact.point, Quaternion.identity),
                    1); //Apparition de particules d'étincelles pendant 1 seconde

            OVRInput.SetControllerVibration(1.0f, 1.0f, OVRInput.Controller.Touch); //Les manettes vibrent fortement

            if (stoneCollisionArea.GetComponent<StoneCollisionArea>().IsStoneInFireArea(otherStone.gameObject)
            ) //Si les pierres entrechoquées sont dans la zone du feu
                if (stoneCollisionArea.GetComponent<StoneCollisionArea>().CountSmallBranchesInFireArea >= 3
                ) //Si le nombre de petites branches dans la zone est supérieur à 3
                    if (!gameManager.GetComponent<GameManager>().IsMission1Completed)
                    {
                        //Si le feu n'a pas déjà été allumé
                        Debug.Log("Création du feu");
                        gameManager.GetComponent<GameManager>().IsMission1Completed = true; //Le feu a été allumé
                        Instantiate(firePrefab, fireCampGround.transform.position,
                            Quaternion.identity); //Le feu est instancié

                        //Création d'une lumière au niveau du feu
                        var light = new GameObject();
                        var lightComp = light.AddComponent<Light>();
                        light.transform.position = fireCampGround.transform.position + new Vector3(0, 0.2F, 0);
                        lightComp.color = Color.yellow;
                    }
        }
        else if (collisionForce > forceAmountToCreateDust
        ) //Si on ne frappe pas assez fortement et/ou que les 2 pierres ne sont pas différentes
        {
            Debug.Log("Pas assez fort!");
            OVRInput.SetControllerVibration(1.0f, 0.5f,
                OVRInput.Controller.Touch); //Les manettes vibrent moyennement fort
            _audioData.PlayOneShot(stoneCollisionSound, 0.05F); //On joue un son spécifique
            foreach (var contact in otherStone.contacts) //Pour chaque point de contact
                Destroy(Instantiate(dustPrefab, contact.point, Quaternion.identity),
                    1); //De la poussière est émise pendant 1 seconde
        }
    }

    /// <summary>
    ///     Cette méthode permet de savoir si la pierre en paramètre est différente de la pierre actuelle.
    /// </summary>
    /// <remarks>
    ///     Pour rappel, une pierre peut être soit Marcassite, soit Silex.
    /// </remarks>
    /// <param name="stone">Pierre à vérifier.</param>
    /// <returns>
    ///     <list type="bullet">
    ///         <item>
    ///             <term>**true** :</term>
    ///             <description>si les pierres qui rentrent en collision sont différentes</description>
    ///         </item>
    ///         <item>
    ///             <term>**false** :</term>
    ///             <description>sinon</description>
    ///         </item>
    ///     </list>
    /// </returns>
    private bool StonesAreDifferent(GameObject stone)
    {
        return stone.GetComponent<Marcassite>() != null && GetComponent<Silex>() != null ||
               stone.GetComponent<Silex>() != null && GetComponent<Marcassite>() != null;
    }
}