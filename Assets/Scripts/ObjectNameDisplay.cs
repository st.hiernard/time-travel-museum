﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
///     Cette classe permet de gérer l'affichage du nom d'un objet.
/// </summary>
public class ObjectNameDisplay : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit du texte qui sera affiché.
    /// </summary>
    public Text objectName;

    // Start is called before the first frame update
    private void Start()
    {
        objectName.text = "";
    }

    /// <summary>
    ///     Cette méthode permet d'afficher le nom de l'objet en fonction du paramètre <paramref name="state" />.
    /// </summary>
    /// <param name="state">Le booléen qui permet détermine si on affiche le nom de l'objet ou non.</param>
    public void DisplayName(bool state)
    {
        if (state)
            objectName.text = name;
        else
            objectName.text = "";
    }
}