﻿using UnityEngine;

/// <summary>
///     Cette classe permet de gérer l'ouverture de la porte de la salle spéciale.
/// </summary>
public class DoorController : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit du composant permettant de gérer les niveaux et les missions.
    /// </summary>
    private LevelManager _levelManager;

    /// <summary>
    ///     Il s'agit de l'état de la porte (ouverte si true, fermée sinon)
    /// </summary>
    private bool _opened;

    // Start is called before the first frame update
    private void Start()
    {
        // On récupère le GameObject puis le composant correspondant au LevelManager.
        var levelManagerGO = GameObject.Find("LevelManager");
        if (levelManagerGO) _levelManager = levelManagerGO.GetComponent<LevelManager>();
    }

    // Update is called once per frame
    private void Update()
    {
        ManageDoorOpening();
    }

    /// <summary>
    ///     Cette méthode permet de mettre à jour l'attribut <see cref="_opened" />,
    ///     et d'activer ou non le GameObject correspondant à la porte.
    /// </summary>
    private void ManageDoorOpening()
    {
        if (_levelManager)
        {
            _opened = _levelManager.IsLevel1Completed;
            Debug.Log(_opened);
        }
        else
        {
            _opened = false;
        }

        gameObject.SetActive(!_opened);
    }
}