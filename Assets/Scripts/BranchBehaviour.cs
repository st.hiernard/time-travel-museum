﻿using System;
using UnityEngine;

public enum Side
{
    Left,
    Right
}

/// <summary>
///     Cette classe permet de gérer le comportement des extrémités d'une branche (qui va devenir une lance).
///     Elle contient notamment la gestion de la collision avec le couteau.
/// </summary>
public class BranchBehaviour : MonoBehaviour
{
    /// <summary>
    ///     Cet attribut permet d'identifier le côté de la branche.
    ///     Il est possible de tailler les deux côtés d'une branche.
    /// </summary>
    public Side side;

    /// <summary>
    ///     Il s'agit ici du prefab pour les particules de poussières.
    /// </summary>
    public GameObject dustPrefab;

    /// <summary>
    ///     Il s'agit ici du prefab pour les particules de sciures.
    /// </summary>
    public GameObject chipsPrefab;

    /// <summary>
    ///     Il s'agit du seuil à partir duquel la branche produit des sciures et est taillée.
    /// </summary>
    public float cutThreshold = 2.5f;

    /// <summary>
    ///     Il s'agit seuil à partir duquel la branche produit de la poussière.
    /// </summary>
    public float dustThreshold = 0.5f;

    /// <summary>
    ///     Il s'agit de l'angle de coupe minimum.
    /// </summary>
    public float minAngle;

    /// <summary>
    ///     Il s'agit de l'angle de coupe maximum.
    /// </summary>
    public float maxAngle = 60;

    /// <summary>
    ///     Cet attribut permet d'accéder au script de comportement de la partie principale de la lance (le parent).
    /// </summary>
    private SpearBehaviour _spearBehaviour;

    // Start is called before the first frame update
    private void Start()
    {
        // On récupère le composant SpearBehaviour dans le parent (càd la branche principale).
        _spearBehaviour = GetComponentInParent<SpearBehaviour>();
    }

    private void OnCollisionEnter(Collision other)
    {
        /* Pour vérifier si l'objet qui rentre en collision avec la branche est bien un couteau,
         on vérifie qu'il contient bien le composant "Cutter".     
         */
        if (other.gameObject.GetComponent<Cutter>()) ManageCutterCollision(other);
    }

    private void OnCollisionExit(Collision collision)
    {
        // On arrête les vibrations de la manette.
        OVRInput.SetControllerVibration(0f, 0f, OVRInput.Controller.Touch);
    }

    /// <summary>
    ///     Cette méthode permet de gérer les collisions avec le couteau.
    ///     Elle est appelée lors de la collision si l'objet
    ///     contient le composant "Cutter".
    /// </summary>
    /// <param name="cutter">L'entité de type Collision correspondant à un couteau.</param>
    private void ManageCutterCollision(Collision cutter)
    {
        // On calcule l'angle de collision.
        var normal = cutter.contacts[0].normal;
        var cutterVelocity = cutter.relativeVelocity;
        var collisionAngle = 90 - Vector3.Angle(cutterVelocity, -normal);
        Debug.Log("Collision Angle : " + collisionAngle);

        // On vérifie que l'angle de collision est bien dans l'intervalle (minAngle,maxAngle).
        var isAngleCorrect = Math.Abs(collisionAngle) >= minAngle && Math.Abs(collisionAngle) <= maxAngle;

        // On récupère la force de collision (ici on utilise la vélocité).
        var collisionForce = cutter.relativeVelocity.magnitude;

        // Si la force est supérieure au seuil cutThreshold et que l'angle de collision est correct...
        if (collisionForce >= cutThreshold && isAngleCorrect)
        {
            // On fait vibrer les manettes (fort).
            OVRInput.SetControllerVibration(1.0f, 0.5f, OVRInput.Controller.Touch);
            Debug.Log("Lance taillée !");
            if (_spearBehaviour)
            {
                // On transforme l'extrémité en pointe par le biais du composant SpearBehaviour.
                _spearBehaviour.ToSpike(side);
                // On émet un son par le biais du composant SpearBehaviour.
                _spearBehaviour.PlaySound("success");
            }

            // On émet des particules (sciures).
            foreach (var contact in cutter.contacts)
                Destroy(Instantiate(chipsPrefab, contact.point, Quaternion.identity),
                    1);
        }
        // Si la force est entre le seuil dustThreshold et cutThreshold...
        else if (collisionForce >= dustThreshold)
        {
            Debug.Log("Presque (pas assez fort ou pas assez incliné) !");
            // On fait vibrer les manettes (faible).
            OVRInput.SetControllerVibration(1.0f, 0.25f, OVRInput.Controller.Touch);

            // On émet des particules (poussière).
            foreach (var contact in cutter.contacts)
                Destroy(Instantiate(dustPrefab, contact.point, Quaternion.identity),
                    1);

            // On émet un son par le biais du composant SpearBehaviour.
            if (_spearBehaviour) _spearBehaviour.PlaySound("fail");
        }
        else
        {
            Debug.Log("Pas assez fort !");
        }
    }
}