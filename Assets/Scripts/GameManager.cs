﻿using UnityEngine;

/// <summary>
///     Cette classe permet de gérer les éléments de jeu (ici du niveau 1),
///     c'est-à-dire l'ouverture du portail de fin et l'ajout de la dernière mission.
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit du prefab du portail pour retourner dans le musée.
    /// </summary>
    public GameObject portalPrefab;

    /// <summary>
    ///     Il s'agit du GameObject correspondant à la grotte.
    /// </summary>
    public GameObject cave;

    /// <summary>
    ///     Cet attribut permet de savoir si la portail a été créé ou non.
    /// </summary>
    private bool _isPortalCreated;

    /// <summary>
    ///     Il s'agit du composant permettant de gérer les niveaux et les missions.
    /// </summary>
    private LevelManager _lvlManager;

    /// <summary>
    ///     Il s'agit du GameOject possédant le composant <see cref="_lvlManager" />.
    /// </summary>
    private GameObject _lvlManagerGO;

    /// <summary>
    ///     Cette propriété correspond à l'état de la mission 1 (complétée ou non)
    /// </summary>
    public bool IsMission1Completed { get; set; }

    /// <summary>
    ///     Cette propriété correspond à l'état de la mission 2 (complétée ou non)
    /// </summary>
    public bool IsMission2Completed { get; set; }

    // Start is called before the first frame update
    private void Start()
    {
        // On récupère le composant LevelManager
        _lvlManagerGO = GameObject.Find("LevelManager");
        if (_lvlManagerGO) _lvlManager = _lvlManagerGO.GetComponent<LevelManager>();
        // On initialise les attributs et les propriétés de type booléen.
        IsMission1Completed = false;
        IsMission2Completed = false;
        _isPortalCreated = false;
    }

    // Update is called once per frame
    private void Update()
    {
        UpdateLevelManagerData();
        ToEndLevel();
    }

    /// <summary>
    ///     Cette méthode permet de mettre à jour les données du <see cref="_lvlManager" />,
    ///     plus particulièrement les états des missions.
    /// </summary>
    private void UpdateLevelManagerData()
    {
        if (_lvlManager)
        {
            _lvlManager.QuestsLabels[0] = (_lvlManager.QuestsLabels[0].Item1, IsMission1Completed);
            _lvlManager.QuestsLabels[1] = (_lvlManager.QuestsLabels[1].Item1, IsMission2Completed);
        }
    }

    /// <summary>
    ///     Cette méthode permet, si le niveau est terminé, de créer le portail permettant de retourner au musée et d'ajouter
    ///     la dernière mission.
    /// </summary>
    /// <remarks>
    ///     Le niveau est terminé lorsque les deux premières missions sont complétées.
    ///     La dernière mission consiste à passer le portail et à visiter la salle spéciale.
    /// </remarks>
    private void ToEndLevel()
    {
        if (IsMission1Completed && IsMission2Completed)
            if (!_isPortalCreated)
            {
                Instantiate(portalPrefab, cave.transform.position + new Vector3(12.486F, 2.6F, 1.72F),
                    Quaternion.identity); //Création du portail
                _isPortalCreated = true;
                if (_lvlManager)
                {
                    _lvlManager.IsLevel1Completed = true; //Informe le LevelManager que ce monde a été complété
                    _lvlManager.AddQuest("Mission Bonus : Prendre le portail et visiter la salle spéciale du musée");
                }
            }
    }
}