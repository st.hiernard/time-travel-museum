﻿using UnityEngine;

/// <summary>
///     Cette classe permet d'avoir un composant utile à l'identification d'une "petite" branche.
///     Si un objet possède ce composant, alors il est assimilé à une "petite" branche,
///     notamment quand il s'agit d'allumer un feu.
/// </summary>
public class SmallBranch : MonoBehaviour
{
}