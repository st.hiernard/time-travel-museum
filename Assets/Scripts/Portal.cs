﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
///     Cette classe permet de gérer le comportement du portail de fin.
/// </summary>
public class Portal : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        // Si le joueur traverse le portail, on charge la scène du musée (en remplaçant la scène actuelle).
        if (other.gameObject.CompareTag("Player")) SceneManager.LoadScene("MuseumScene", LoadSceneMode.Single);
    }
}