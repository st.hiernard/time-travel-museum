﻿using UnityEngine;

/// <summary>
///     Cette classe a été créée pour gérer les interactions avec les contrôleurs en VR.
///     Il a été nécessaire de faire un script personnalisé,
///     car nous souhaitions que les objets que nous tenons puissent
///     rentrer en collision, ce qui n'était pas possible avec des scripts classiques.
/// </summary>
/// <remarks>Pour fonctionner, il faut ajouter un collider avec isTrigger=true.</remarks>
public class CustomGrabber : MonoBehaviour
{
    // Variables publiques qui peuvent être définies pour LTouch ou RTouch dans l'inspecteur de Unity.
    /// <summary>
    ///     Permet de sélectionner le controller concerné (LTouch ou RTouch)
    /// </summary>
    public OVRInput.Controller controller = OVRInput.Controller.None;

    /// <summary>
    ///     Material du grabber lorsqu'on peut saisir un objet.
    /// </summary>
    public Material grabbingZoneMaterial;

    /// <summary>
    ///     Material du grabber lorsqu'on saisit un objet.
    /// </summary>
    public Material grabbingMaterial;

    /// <summary>
    ///     Material du grabber par défaut.
    /// </summary>
    public Material defaultMaterial;

    /// <summary>
    ///     Il s'agit de l'objet avec qui le grabber rentre en collision.
    /// </summary>
    private GameObject _collidingObject;

    /// <summary>
    ///     Cet attribut permet de savoir si le bouton de Grab est pressé ou non.
    /// </summary>
    private bool _isButtonGrabPressed;

    /// <summary>
    ///     Cet attribut permet de savoir si le bouton de Grab était pressé ou non à la dernière mise à jour.
    /// </summary>
    private bool _lastUpdateIsButtonGrabPressed;

    /// <summary>
    ///     Il s'agit de l'objet que l'on tient dans la main
    /// </summary>
    private GameObject _objectInHand;

    private void Start()
    {
        // On récupère le material initialement affecté.
        defaultMaterial = GetComponent<Renderer>().material;
    }


    private void Update()
    {
        UpdateGrabbing();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Grabbable")) //Si un object saisissable est triggered
            ManageGrabbableTriggerEnter(other);
    }


    public void OnTriggerExit(Collider other)
    {
        if (_objectInHand == null)
        {
            GetComponent<Renderer>().material = defaultMaterial; //On change la couleur de la main
            if (_collidingObject.GetComponent<ObjectNameDisplay>() != null)
                _collidingObject.GetComponent<ObjectNameDisplay>().DisplayName(false); //On enlève le nom de l'objet
        }

        _collidingObject = null;
    }

    /// <summary>
    ///     Cette méthode permet de gérer  le fait qu'un objet de type Grabbable rentre dans la zone de trigger.
    ///     Elle est appelée lorsque l'objet possède le tag "Grabbable".
    /// </summary>
    /// <param name="grabbable">L'entité de type Collider correspondant à un objet de type Grabbable.</param>
    private void ManageGrabbableTriggerEnter(Collider grabbable)
    {
        _collidingObject = grabbable.gameObject;
        if (_objectInHand == null)
        {
            GetComponent<Renderer>().material = grabbingZoneMaterial; //On change la couleur de la main
            if (_collidingObject.GetComponent<ObjectNameDisplay>() != null)
                _collidingObject.GetComponent<ObjectNameDisplay>().DisplayName(true); //On affiche le nom de l'objet
        }
    }

    /// <summary>
    ///     Cette méthode permet de mettre à jour tout ce qui concerne l'éventuelle saisie d'un d'un nouvel objet.
    /// </summary>
    private void UpdateGrabbing()
    {
        OVRInput.Update(); //Met à jour les inputs
        UpdateIsButtonGrabPressed(); //Met à jour l'état du booléen IsButtonGrabPressed
        if (_objectInHand != null)
            if (_objectInHand.GetComponent<FixedJoint>() != null)
                if (_objectInHand.GetComponent<FixedJoint>().connectedBody != GetComponent<Rigidbody>()
                ) //Vérifie qu'il n'y a pas d'objets en saisi de stocké si l'objet en collision est déjà grab par une autre main
                    _objectInHand = null;
        if (_isButtonGrabPressed && !_lastUpdateIsButtonGrabPressed && _collidingObject && _objectInHand == null
        ) //Si on appuie sur le bouton de saisie et qu'un objet peut être saisi
            GrabObject(); //On attrape l'objet
        else if (!_isButtonGrabPressed) //Sinon si on relache le bouton de saisie
            ReleaseObject(); //On relache l'objet
    }

    /// <summary>
    ///     Cette méthode permet de saisir un objet.
    /// </summary>
    public void GrabObject()
    {
        _objectInHand = _collidingObject;
        if (_collidingObject.GetComponent<ObjectNameDisplay>() != null)
            _collidingObject.GetComponent<ObjectNameDisplay>().DisplayName(false); //On retire le nom de l'objet
        if (_objectInHand.GetComponent<FixedJoint>() == null)
            _objectInHand.AddComponent<FixedJoint>(); //Si l'objet saisi n'a pas de FixedJoint, on en ajoute un
        _objectInHand.GetComponent<FixedJoint>().connectedBody =
            GetComponent<Rigidbody>(); // On relie avec un Fixed Joint l'objet saisi avec la main

        GetComponent<Renderer>().material = grabbingMaterial; //On change la couleur de la main
    }


    /// <summary>
    ///     Cette méthode permet de relâcher un objet
    /// </summary>
    private void ReleaseObject()
    {
        if (_objectInHand != null &&
            _objectInHand.GetComponent<FixedJoint>().connectedBody == GetComponent<Rigidbody>())
            Destroy(_objectInHand.GetComponent<FixedJoint>()); //On détruit le lien entre la main et l'objet saisi
        if (_collidingObject == null)
            GetComponent<Renderer>().material = defaultMaterial; //On change la couleur de la main
        _objectInHand = null;
    }

    /// <summary>
    ///     Cette méthode permet de mettre à jour les variables <see cref="_isButtonGrabPressed" /> et
    ///     <see cref="_lastUpdateIsButtonGrabPressed" />.
    /// </summary>
    private void UpdateIsButtonGrabPressed()
    {
        _lastUpdateIsButtonGrabPressed = _isButtonGrabPressed; //dernier état du bouton avant un update
        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, controller) > 0f) //Si le bouton est pressé
            _isButtonGrabPressed = true;
        else //Si le bouton est relaché
            _isButtonGrabPressed = false;
    }
}