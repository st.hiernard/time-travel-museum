﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     Cette classe permet de gérer le comportement de la zone dans laquelle les pierres doivent entrer en collision
///     pour allumer un feu.
///     Il s'agit également de la zone dans laquelle la pointe de la lance doit rester pour être brûlée.
/// </summary>
public class StoneCollisionArea : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit de la liste des pierres présentes dans la zone.
    /// </summary>
    private List<GameObject> _stonesInFireArea;

    /// <summary>
    ///     Il s'agit du nombre de "petites" branches dans la zone.
    /// </summary>
    public int CountSmallBranchesInFireArea { get; set; }

    // Start is called before the first frame update
    private void Start()
    {
        // On initialise la liste des pierres et le nombre de branches.
        _stonesInFireArea = new List<GameObject>();
        CountSmallBranchesInFireArea = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Stone>() != null) //Si l'objet dans la zone de trigger est une pierre
        {
            Debug.Log("Une pierre est dans la zone de feu");
            _stonesInFireArea.Add(other.gameObject); //Ajoute la pierre à la liste des pierres dans la zone
        }

        if (other.gameObject.GetComponent<SmallBranch>() != null
        ) //Si l'objet dans la zone de trigger est une petite branche
        {
            Debug.Log("Une branche est dans la zone de feu");
            CountSmallBranchesInFireArea++; //Ajoute la branche à la liste des petites branches dans la zone
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Stone>() != null)
        {
            Debug.Log("Une pierre sort de la zone de feu");
            _stonesInFireArea.Remove(other.gameObject); //Enlève la pierre à la liste des pierres dans la zone
        }

        if (other.gameObject.GetComponent<SmallBranch>() != null
        ) //Si l'objet dans la zone de trigger est une petite branche
        {
            Debug.Log("Une branche sort de la zone de feu");
            CountSmallBranchesInFireArea--; //Enlève la branche à la liste des petites branches dans la zone
        }
    }

    /// <summary>
    ///     Cette méthode permet de savoir si la pierre (<paramref name="stone" />
    ///     passée en paramètre est dans la zone ou non.
    /// </summary>
    /// <param name="stone">Pierre dont on souhaite vérifier la présence dans la zone.</param>
    /// <returns>
    ///     <list type="bullet">
    ///         <item>
    ///             <term>**true** :</term>
    ///             <description>si la pierre est dans la zone</description>
    ///         </item>
    ///         <item>
    ///             <term>**false** :</term>
    ///             <description>sinon</description>
    ///         </item>
    ///     </list>
    /// </returns>
    public bool IsStoneInFireArea(GameObject stone)
    {
        return _stonesInFireArea.Contains(stone);
    }
}