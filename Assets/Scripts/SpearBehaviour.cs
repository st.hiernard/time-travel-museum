﻿using UnityEngine;

/// <summary>
///     Cette classe permet de gérer le comportement d'une grande branche (qui va devenir une lance).
///     Cette classe contient notamment la méthode permettant de transformer les extrémités en pointes.
/// </summary>
public class SpearBehaviour : MonoBehaviour
{
    /// <summary>
    ///     Il s'agit du clip audio à jouer en cas de **succès** (branche taillée).
    /// </summary>
    public AudioClip successSound;

    /// <summary>
    ///     Il s'agit du clip audio à jouer en cas d'**échec** (branche non taillée).
    /// </summary>
    public AudioClip failSound;

    /// <summary>
    ///     Il s'agit du GameObject correspondant à la branche qui sert de base à la lance.
    /// </summary>
    public GameObject branch;

    /// <summary>
    ///     Il s'agit du composant AudioSource chargé de jouer les sons.
    /// </summary>
    private AudioSource _audioSource;

    /// <summary>
    ///     Il s'agit du GameObject qui sert d'extrémité **gauche avant** transformation.
    /// </summary>
    private GameObject _branchGOLeft;

    /// <summary>
    ///     Il s'agit du GameObject qui sert d'extrémité **droite avant** transformation.
    /// </summary>
    private GameObject _branchGORight;

    /// <summary>
    ///     Il s'agit du GameObject qui sert d'extrémité **gauche après** transformation.
    /// </summary>
    private GameObject _spikeGOLeft;

    /// <summary>
    ///     Il s'agit du GameObject qui sert d'extrémité **droite après** transformation.
    /// </summary>
    private GameObject _spikeGORight;

    // Start is called before the first frame update
    private void Start()
    {
        // On récupère le composant AudioSource
        _audioSource = GetComponent<AudioSource>();
        InitLeftExt();
        InitRightExt();
    }

    /// <summary>
    ///     Cette méthode permet de récupérer les GameObject pour l'extrémité **gauche**.
    ///     Elle permet également de donner l'état initiale des GameObject.
    /// </summary>
    /// <remarks>
    ///     A l'état initial, c'est une branche non taillée qui sert d'extrémité.
    ///     A l'état final, c'est une branche taillée (pointe) qui sert d'extrémité.
    /// </remarks>
    private void InitLeftExt()
    {
        _spikeGOLeft = branch.transform.Find("ext-spike-1").gameObject;
        _branchGOLeft = branch.transform.Find("ext-branch-1").gameObject;
        _spikeGOLeft.SetActive(false);
        _branchGOLeft.SetActive(true);
    }

    /// <summary>
    ///     Cette méthode permet de récupérer les GameObject pour l'extrémité **droite**.
    ///     Elle permet également de donner l'état initiale des GameObject.
    /// </summary>
    /// <remarks>
    ///     A l'état initial, c'est une branche non taillée qui sert d'extrémité.
    ///     A l'état final, c'est une branche taillée (pointe) qui sert d'extrémité.
    /// </remarks>
    private void InitRightExt()
    {
        _spikeGORight = branch.transform.Find("ext-spike-2").gameObject;
        _branchGORight = branch.transform.Find("ext-branch-2").gameObject;
        _spikeGORight.SetActive(false);
        _branchGORight.SetActive(true);
    }

    /// <summary>
    ///     Cette méthode permet de transformer une extrémité (indiquée par <paramref name="side" />) de la branche en pointe.
    /// </summary>
    /// <param name="side">Extrémité de la branche.</param>
    public void ToSpike(Side side)
    {
        if (side == Side.Left)
        {
            _branchGOLeft.SetActive(false);
            _spikeGOLeft.SetActive(true);
        }
        else if (side == Side.Right)
        {
            _branchGORight.SetActive(false);
            _spikeGORight.SetActive(true);
        }
    }

    /// <summary>
    ///     Cette méthode permet d'émettre un son en fonction d'un <paramref name="type" /> passé en paramètre.
    /// </summary>
    /// <param name="type">Type du son à émettre ("success" ou "fail").</param>
    public void PlaySound(string type)
    {
        if (type == "success")
        {
            _audioSource.clip = successSound;
            _audioSource.Play();
        }
        else if (type == "fail")
        {
            _audioSource.clip = failSound;
            _audioSource.Play();
        }
    }
}