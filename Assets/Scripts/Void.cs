﻿using UnityEngine;

/// <summary>
///     Cette classe permet de gérer le comportement des objets (y compris le joueur)
///     qui passent à travers le sol / tombent dans le vide par accident.
/// </summary>
/// <remarks>
///     Une zone de trigger a été mise en place pour détecter le passage des objets.
///     Il s'agit de réinitialiser la position des objets à leur position de départ.
/// </remarks>
/// <seealso cref="PositionResetter" />
public class Void : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PositionResetter>() != null) //Si l'objet possède le composant PositionRestter
        {
            // Besoin d'un traitement spécifique pour le first person controller
            if (other.gameObject.GetComponent<CharacterController>() != null)
                ManageCharacterControllerTrigger(other);
            // Pour le reste, comportement normal
            else
                ManageOtherTrigger(other);
        }
    }

    /// <summary>
    ///     Cette méthode permet de gérer le trigger quand il s'agit un CharacterController.
    /// </summary>
    /// <remarks>
    ///     La gestion est différente d'un objet classique, notamment à cause de script du CharacterController.
    /// </remarks>
    /// <param name="ccCollider">Le CharacterController qui rentre dans la zone de trigger.</param>
    private void ManageCharacterControllerTrigger(Collider ccCollider)
    {
        ccCollider.gameObject.GetComponent<CharacterController>().enabled = false;
        ccCollider.gameObject.GetComponent<Rigidbody>().useGravity = false;
        ccCollider.gameObject.GetComponent<PositionResetter>().ResetPosition();
        ccCollider.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ccCollider.gameObject.GetComponent<Rigidbody>().useGravity = true;
        ccCollider.gameObject.GetComponent<CharacterController>().enabled = true;
    }

    /// <summary>
    ///     Cette méthode permet de gérer le trigger quand il s'agit d'un objet classique.
    /// </summary>
    /// <param name="objCollider">L'objet qui rentre dans la zone de trigger.</param>
    private void ManageOtherTrigger(Collider objCollider)
    {
        objCollider.gameObject.GetComponent<Rigidbody>().useGravity = false;
        objCollider.gameObject.GetComponent<PositionResetter>().ResetPosition();
        objCollider.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        objCollider.gameObject.GetComponent<Rigidbody>().useGravity = true;
    }
}