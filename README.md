# Time Travel Museum

Projet de réalité virtuelle réalisée avec Unity3D et un casque OculusQuest.

Créateurs : 

- Stéphane HIERNARD
- Florentin Vanaldewereld

## Description

Time Travel Museum est une application de réalité virtuelle mettant en scène un musée futuriste dans lequel l’utilisateur peut accéder à des simulations de différentes époques. Le musée contient des affiches et des images permettant de transmettre des connaissances à réutiliser pour réaliser diverses activités dans des simulations historiques. Dans le cadre de notre projet de RV01, nous avons limité notre projet à l’époque préhistorique.

Après apparition dans le musée, vous pourrez explorer ce dernier et prendre des notes en vous plaçant sous des affiches explicatives. En appuyant sur le bouton noir au fond de la salle du musée, vous serez transporté dans un monde préhistorique afin de réaliser deux missions principales :

- Allumer un feu
- Tailler un épieu

C’est en vous aidant des notes que vous retrouverez dans la simulation et en explorant celle-ci que vous parviendrez à compléter ces missions principales.

Chaque mission met en avant différentes interactions comme le fait de frapper plus ou moins fort certaines pierres afin de générer des étincelles, ou bien de frapper un couteau sur une longue branche selon un certain angle pour concevoir un épieu.

Chaque mission se décompose en deux sous-missions :

- Pour allumer un feu, il vous faudra ramasser des branches sèches à déposer dans un feu de camp, et frapper assez fort un silex contre une marcassite pour produire des étincelles au-dessus de celui-ci.
- Pour tailler un épieu, il vous faudra frapper assez fort un couteau sur le bout d’une branche selon un angle incliné, et durcir la pointe ainsi taillée en la laissant quelques secondes dans du feu.

Après avoir complété les missions, un portail apparaîtra et vous permettra de retourner dans le musée. Vous pourrez ainsi découvrir le contenu d’une salle qui était jusqu’alors fermée. 

## Installation

Cloner le projet et l'ouvrir avec Unity3D.

## Présentation

[![miniature](./res/miniature.jpg)](https://youtu.be/mWw5RIRvZUg)

